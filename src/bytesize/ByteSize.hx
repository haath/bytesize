package bytesize;


/**
 * Type representing a size in bytes.
 */
enum abstract ByteSize(Float)
{
    /** One byte. */
    var B = 1;
    /** One kilobyte (1000 bytes). */
    var KB = 1000;
    /** One megabyte (1000 kilobyte). */
    var MB = 1000000;
    /** One gigabyte (1000 megabyte). */
    var GB = 1000000000;
    /** One terabyte (1000 gigabyte). */
    var TB = 1000000000000;
    /** One petabyte (1000 terabyte). */
    var PB = 1000000000000000;
    /** One exabyte (1000 petabyte). */
    var EB = 1000000000000000000;
    /** One kibibyte (1024 bytes). */
    var KiB = 1024;
    /** One mebibyte (1024 kibibyte). */
    var MiB = 1048576;
    /** One gibibyte (1024 mebibyte). */
    var GiB = 1073741824;
    /** One tebibyte (1024 gibibyte). */
    var TiB = 1099511627776;
    /** One pebibyte (1024 tebibyte). */
    var PiB = 1125899906842624;
    /** One exbibyte (1024 pebibyte). */
    var EiB = 1152921504606846976;


    /**
     * Convert the byte size to a human-readable string.
     *
     * @param decimals the number of decimals to round the output number to
     * @param unit the base unit to use
     * @return the human-readable string
     */
    public function toString(decimals: Int = 0, unit: ByteUnit = Decimal): String
    {
        // initialize value to bytes
        var value: Float = cast this;

        var prefixIndex: Int = 0;
        var base: Float = switch unit
        {
            case Decimal: 1000;
            case Binary: 1024;
        }
        while (Math.abs(value) >= base)
        {
            value /= base;
            prefixIndex++;
        }

        // round and get prefix to the unit
        var valueRounded: Float;
        if (decimals > 0)
        {
            var roundMult: Float = Math.pow(10, decimals);
            valueRounded = Math.round(value * roundMult) / roundMult;
        }
        else
        {
            valueRounded = Math.round(value);
        }
        var unitPrefix: String = prefixIndex > 0 ? "KMGTPE".charAt(prefixIndex - 1) : "";

        if (unit == Binary && prefixIndex > 0)
        {
            unitPrefix += "i";
        }

        return '${valueRounded} ${unitPrefix}B';
    }


    /**
     * Parses a `ByteSize` from a human-readable string.
     *
     * Some examples of parsable strings are:
     * - `25`
     * - `5 KB`
     * - `3.4mib`
     * - `12.4 pebibytes`
     *
     * An exception is thrown if a byte size cannot be parsed from the given string.
     *
     * @param str the string to parse
     * @return the byte size parsed from the string
     */
    public static function parse(str: String): ByteSize
    {
        str = str.toLowerCase();
        var ereg: EReg = ~/^([+-]?(?:[0-9]*[.])?[0-9]+)?\s*(\w+)?$/;

        if (!ereg.match(str))
        {
            throw 'unable to parse: $str';
        }

        var value: Float = Std.parseFloat(ereg.matched(1));
        var unit: String = ereg.matched(2);

        var unitMult: ByteSize = switch unit
        {
            case null | "": B;
            case "kb" | "kilobyte" | "kilobytes": KB;
            case "mb" | "megabyte" | "megabytes": MB;
            case "gb" | "gigabyte" | "gigabytes": GB;
            case "pb" | "petabyte" | "petabytes": PB;
            case "eb" | "exabyte"  | "exabytes":  EB;

            case "kib" | "kibibyte" | "kibibytes": KiB;
            case "mib" | "mebibyte" | "mebibytes": MiB;
            case "gib" | "gibibyte" | "gibibytes": GiB;
            case "tib" | "tebibyte" | "tebibytes": TiB;
            case "pib" | "pebibyte" | "pebibytes": PiB;
            case "eib" | "exbibyte" | "exbibytes": EiB;

            default: throw 'unable to parse: $str';
        }

        return value * unitMult;
    }


    @:op(A * B) @:commutative
    static inline function mult(u: ByteSize, v: Float): ByteSize
    {
        return cast(v * cast(u, Float), ByteSize);
    }


    @:op(A / B)
    static inline function div(s: ByteSize, d: Float): ByteSize
    {
        return cast(cast(s, Float) / d, ByteSize);
    }


    @:op(A / B)
    static inline function ratio(s1: ByteSize, s2: ByteSize): Float
    {
        return cast(s1, Float) / cast(s2, Float);
    }


    @:op(A + B)
    static inline function add(s1: ByteSize, s2: ByteSize): ByteSize
    {
        return cast(cast(s1, Float) + cast(s2, Float), ByteSize);
    }


    @:op(A - B)
    static inline function sub(s1: ByteSize, s2: ByteSize): ByteSize
    {
        return cast(cast(s1, Float) - cast(s2, Float), ByteSize);
    }
}
