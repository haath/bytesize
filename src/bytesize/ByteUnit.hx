package bytesize;


enum ByteUnit
{
    /**
     * Byte unit based on powers of 10.
     */
    Decimal;

    /**
     * Byte unit based on powers of 2.
     */
    Binary;
}
