<div align="center">

[![ ](https://gitlab.com/haath/bytesize/-/raw/master/assets/logo.png)](https://gitlab.com/haath/bytesize)

[![ ](https://gitlab.com/haath/bytesize/badges/master/pipeline.svg)](https://gitlab.com/haath/bytesize/pipelines)
[![ ](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://gitlab.com/haath/bytesize/blob/master/LICENSE)

</div>

- Enables type-safe and readable byte size variables.
- Simple Haxe abstract over `Float` &rarr; no overhead.
- Convert sizes to human-readable strings (i.e `40.5 KB`).
- Parse sizes from human-readable strings.


## Installation

```sh
haxelib install bytesize
```


## Usage

```haxe
var s: ByteSize = 5*MB + 213*KB;

trace('$s');          // 5 MB
trace(s.toString(1)); // 5.2 MB
```

```haxe
var s: ByteSize = ByteSize.parse("123.4 kilobyte");

trace(s.toString(1)); // 123.4 KB
```

Math operations can be done with byte sizes.

```haxe
var ratio: Float = (6 * MB) / (3 * MB); // 2.0

var div: ByteSize = (5 * GB) / 5;       // 1 GB
```

The binary power-2 units are also supported.

```haxe
var s: ByteSize = 1024*B + 1*KiB;

trace(s.toString(Binary)); // 2 KiB
```
