package tests;

import utest.Assert;
import utest.ITest;

import bytesize.ByteSize;
import bytesize.ByteSize.*;


class ByteSizeTest implements ITest
{
    public function new() { }


    function testComprehension()
    {
        Assert.equals(3000, 3 * KB);

        Assert.equals(4000 * KB, 4 * MB);
    }


    function testToString()
    {
        Assert.equals("5 B", '${5 * B}');
        Assert.equals("6 KB", '${6.43 * KB}');
        Assert.equals("6.4 KB", (6.43 * KB).toString(1));
        Assert.equals("6.43 KB", (6.43 * KB).toString(2));

        Assert.equals("1 KiB", (1024 * B).toString(0, Binary));
        Assert.equals("1023 B", (1023 * B).toString(0, Binary));
    }


    function testDiv()
    {
        Assert.equals(3, 30 * B / 10);
        Assert.equals("500 KB", (MB / 2).toString());
    }


    function testRatio()
    {
        Assert.floatEquals(0.5, (500 * KB) / MB);
        Assert.floatEquals(2, (2 * GB) / GB);
        Assert.floatEquals(3500, 3.5 * GB / MB);
    }


    function testAdd()
    {
        Assert.equals(1500, KB + 500*B);
        Assert.equals(5865000, 5*MB + 865*KB);
    }


    function testParse()
    {
        Assert.equals(5 * MB, ByteSize.parse("5 MB"));
        Assert.equals(5.25 * MB, ByteSize.parse("5.25 MB"));
        Assert.equals(123.0 * GiB, ByteSize.parse("123 gibibytes"));
        Assert.equals(123.0 * MiB, ByteSize.parse("123mebibyte"));
        Assert.equals(5.25, ByteSize.parse("5.25"));

        Assert.raises(() -> ByteSize.parse("hello world"), String);
    }
}
